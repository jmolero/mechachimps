
var gulp = require('gulp');
var ts = require('gulp-typescript');
var concat = require('gulp-concat');
var sourcemaps = require('gulp-sourcemaps');

var tsProject = ts.createProject('tsconfig.json');

gulp.task('typescript', function() {
    var tsResult = gulp.src([
    						  'src/**/**.ts',
    						  'phaser/typescript/p2.d.ts',
    						  'phaser/typescript/pixi.d.ts',
    						  'phaser/typescript/phaser.d.ts',
    						  'typings/**/*.d.ts'
    						])
                  .pipe(sourcemaps.init()) // This means sourcemaps will be generated

                  .pipe(ts(tsProject));

       tsResult.js
       			      .pipe(concat('mechachimps.js')) // You can use other plugins that also support gulp-sourcemaps
                  .pipe(sourcemaps.write("../js")) // Now the sourcemaps are added to the .js file
                  .pipe(gulp.dest('js'));
});

gulp.task('watch', ['typescript'], function() {
    gulp.watch('src/**/*.ts', ['typescript']);
});
