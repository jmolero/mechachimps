/// <reference path="../phaser/typescript/phaser.d.ts"/>
// /// <reference path="Boot.ts"/>
// /// <reference path="scenes/preload/PreLoader.ts"/>
// /// <reference path="scenes/SplashState.ts"/>
// /// <reference path="scenes/CreditsState.ts"/>
// /// <reference path="scenes/StatsState.ts"/>
// /// <reference path="scenes/DevelopmentLevelSelection.ts"/>

module MechaChimps {

    export class Game extends Phaser.Game {

        public static _instance: Game;

        constructor() {
            super(768, 480, Phaser.AUTO , "game", { preload: this.preload, create: this.create });
            Game._instance = this;

            // this.state.add('Boot', Boot, true);
            // this.state.add('PreLoader', PreLoader, false);

            // this.state.add('SplashState', SplashState, false);
            // this.state.add('CreditsState', CreditsState, false);

            // if( GameConstants.DEVELOPMENT || GameConstants.HAS_SECRET_BUTTON ){
            //     this.state.add('DevelopmentLevelSelection', DevelopmentLevelSelection, false);
            // }

            // this.state.start('Boot');
        }

        public preload(): void {

            this.load.image("logo", "assets/phaser.png");

        }

        public create(): void {

            var logo: Phaser.Sprite = this.add.sprite(this.world.centerX, this.world.centerY, "logo");
            logo.anchor.setTo(0.5, 0.5);

        }

    }
}
