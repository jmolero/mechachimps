/// <reference path="../phaser/typescript/phaser.d.ts"/>
// /// <reference path="Boot.ts"/>
// /// <reference path="scenes/preload/PreLoader.ts"/>
// /// <reference path="scenes/SplashState.ts"/>
// /// <reference path="scenes/CreditsState.ts"/>
// /// <reference path="scenes/StatsState.ts"/>
// /// <reference path="scenes/DevelopmentLevelSelection.ts"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};
var MechaChimps;
(function (MechaChimps) {
    var Game = (function (_super) {
        __extends(Game, _super);
        function Game() {
            _super.call(this, 768, 480, Phaser.AUTO, "game", { preload: this.preload, create: this.create });
            Game._instance = this;
        }
        Game.prototype.preload = function () {
            this.load.image("logo", "assets/phaser.png");
        };
        Game.prototype.create = function () {
            var logo = this.add.sprite(this.world.centerX, this.world.centerY, "logo");
            logo.anchor.setTo(0.5, 0.5);
        };
        return Game;
    })(Phaser.Game);
    MechaChimps.Game = Game;
})(MechaChimps || (MechaChimps = {}));
/// <reference path="Game.ts"/>
window.onload = function () {
    var game = new MechaChimps.Game();
};
//# sourceMappingURL=mechachimps.js.map