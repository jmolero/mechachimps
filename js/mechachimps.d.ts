/// <reference path="../phaser/typescript/phaser.d.ts" />
declare module MechaChimps {
    class Game extends Phaser.Game {
        static _instance: Game;
        constructor();
        preload(): void;
        create(): void;
    }
}
